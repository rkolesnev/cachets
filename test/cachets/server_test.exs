defmodule Cachets.ServerTest do
  use ExUnit.Case

  test "can be started via supervisor" do
    children = [{Cachets.Server, for: TestCache}]

    {:ok, sup} = Supervisor.start_link(children, strategy: :one_for_one)
    :ok = Supervisor.stop(sup)
  end

  describe "start_link/1" do
    test "raises MatchError when option `for` is missing" do
      assert_raise MatchError, fn ->
        Cachets.Server.start_link([])
      end
    end
  end
end
