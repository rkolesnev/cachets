defmodule CachetsTest do
  use ExUnit.Case
  doctest Cachets

  defmodule TestCache do
    use Cachets
  end

  def start_test_cache(_ctx) do
    start_supervised!({Cachets.Server, for: TestCache})
    :ok
  end

  describe "get/1" do
    setup :start_test_cache

    test "returns `:not_found` when key does not exist" do
      assert :not_found == TestCache.get("not-exists")
    end

    test "returns value stored under key" do
      :ok = TestCache.put("KEY", "VALUE")

      assert {:ok, "VALUE"} == TestCache.get("KEY")
    end
  end

  describe "put/2" do
    setup :start_test_cache

    test "stores value for given key" do
      assert :ok == TestCache.put("KEY", "VALUE")

      assert {:ok, "VALUE"} == TestCache.get("KEY")
    end

    test "overwrites value for given key" do
      assert :ok == TestCache.put("KEY", "VALUE")
      assert :ok == TestCache.put("KEY", "VALUE_NEW")

      assert {:ok, "VALUE_NEW"} == TestCache.get("KEY")
    end
  end

  describe "pop/1" do
    setup :start_test_cache

    test "returns `:not_found` when key does not exist" do
      assert :not_found == TestCache.pop("SOME_KEY")
    end

    test "returns value for given key and removes it from cache" do
      assert :ok == TestCache.put("SOME_KEY", "SOME_VALUE")

      assert {:ok, "SOME_VALUE"} == TestCache.pop("SOME_KEY")
      assert :not_found == TestCache.pop("SOME_KEY")
    end
  end

  describe "fetch/2" do
    setup :start_test_cache

    test "returns `{:ok, value}` when key exists in a cache" do
      :ok = TestCache.put("KEY", "VALVALVAL")
      {:ok, "VALVALVAL"} = TestCache.fetch("KEY", fn -> "AAA" end)
    end

    test "returns `{:ok, value_from_fallaback_fn}` when key is missing in a cache" do
      {:ok, "AAA"} = TestCache.fetch("KEY", fn -> "AAA" end)
    end

    test "when multiple parallel fetches started - fallback function executed only for one fetch" do
      parent = self()

      fallback_fn = fn ->
        # we really need this to simulate parallel fetches
        Process.sleep(10)
        send(parent, :fallback_fn_called)
        "VALUE"
      end

      1..10
      |> Enum.map(fn _ ->
        Task.async(fn ->
          {:ok, value} = TestCache.fetch(:key, fallback_fn)
          value
        end)
      end)
      |> Task.await_many()
      |> Enum.each(fn result ->
        assert result == "VALUE"
      end)

      assert_received :fallback_fn_called
      refute_received _
    end
  end
end
