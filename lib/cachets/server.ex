defmodule Cachets.Server do
  @moduledoc false

  use GenServer

  alias Cachets.Ets

  def start_link(opts) do
    {:ok, cachets_mod} = Keyword.fetch(opts, :for)

    GenServer.start_link(__MODULE__, opts, name: cachets_mod)
  end

  @impl true
  def init(opts) do
    cachets_mod = opts |> Keyword.get(:for)

    {:ok, %{ets: Ets.new(cachets_mod), mod: cachets_mod}}
  end

  @impl true
  def handle_call({:put, key, value}, _from, %{ets: ets} = state) do
    {
      :reply,
      Ets.put(ets, key, value),
      state
    }
  end

  @impl true
  def handle_call({:pop, key}, _from, %{ets: ets} = state) do
    {
      :reply,
      Ets.pop(ets, key),
      state
    }
  end

  @impl true
  def handle_call({:fetch, key, fallback_fn}, _from, %{ets: ets} = state) do
    {
      :reply,
      Ets.fetch(ets, key, fallback_fn),
      state
    }
  end
end
