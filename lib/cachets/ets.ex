defmodule Cachets.Ets do
  @moduledoc false

  @type key :: any()
  @type value :: any()

  def new(name) do
    :ets.new(name, [:set, :protected, :named_table, {:read_concurrency, true}])
  end

  @spec put(:ets.tab(), key(), value()) :: :ok
  def put(ets, key, value) do
    :ets.insert(ets, {key, value})
    :ok
  end

  @spec pop(:ets.tab(), key()) :: {:ok, value()} | :not_found
  def pop(ets, key) do
    case :ets.take(ets, key) do
      [{_, value}] -> {:ok, value}
      [] -> :not_found
    end
  end

  @spec fetch(:ets.tab(), key(), (() -> value())) :: {:ok, value()}
  def fetch(ets, key, fallback_fn) do
    case :ets.lookup(ets, key) do
      [{_, value}] ->
        {:ok, value}

      _ ->
        value = fallback_fn.()
        :ets.insert(ets, {key, value})
        {:ok, value}
    end
  end
end
