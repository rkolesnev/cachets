defmodule Cachets do
  @moduledoc """
  A behaviour module for implementing an [`:ets`](`:ets`)-based cache.

  To define a cache create a module in the following way:

      defmodule MyApp.MyCache do
        use Cachets
      end

  Also you need to start a `Cachets.Server` for you cache:

      Supervisor.start_link(
        [{Cachets.Server, for: MyApp.MyCache}]
      )


  `MyApp.MyCache` will have full implenetation of `Cachets` behaviour.
  So, you can use your cache like:

      MyApp.MyCache.put(:key, "value")
      {:ok, val} = MyApp.MyCache.get(:key)

  Only callback implementation functions will be added by `use/2`,
  so such module is safe for extending with custom functions.
  """

  @type key :: any()
  @type value :: any()

  @doc """
  Returns value stored in the cache under the given `key`.

  This function is a process-local lookup in an [`:ets`](`:ets`) table.
  It allows to have concurrent reads from the cache.
  """
  @callback get(key()) :: {:ok, value()} | :not_found

  @doc """
  Stores `value` under the given `key`.

  This function is a `call` to the cache's GenServer.
  It means that no parallel mutations to the cache is possible.
  """
  @callback put(key(), value()) :: :ok

  @doc """
  Returns `value` stored under the given `key` and deletes it from the cache.

  This function is a `call` to the cache's GenServer.
  It means that no parallel mutations to the cache is possible.
  """
  @callback pop(key()) :: {:ok, value()} | :not_found

  @doc """
  Returns value stored under the given `key` or initializes it using `fallback_fn`.

  This function works in the following way:

  1. tries to get value via process-local ets lookup
  2. if no value found makes a call to the cache's GenServer.
     Inside this call on the server side:
      1. tries to get again
      2. and if ets table still has no such key - generates it via `fallback_fn`

  Because of step 2-1 this approach allows to avoid unncecessary executions of `fallback_fn`
  when we have a lot of parallel fetches for the same key.

  Because of step 1 we still have ability of concurrent reads when value exists in the cache.
  """
  @callback fetch(key(), fallback_fn :: (() -> value())) :: {:ok, value()}

  defmacro __using__(_opts) do
    quote do
      @behaviour Cachets

      @impl true
      def get(key) do
        case :ets.lookup(__MODULE__, key) do
          [{_, value}] -> {:ok, value}
          _ -> :not_found
        end
      end

      @impl true
      def put(key, value) do
        GenServer.call(__MODULE__, {:put, key, value})
      end

      @impl true
      def pop(key) do
        GenServer.call(__MODULE__, {:pop, key})
      end

      @impl true
      def fetch(key, fallback_fn) do
        case get(key) do
          {:ok, value} -> {:ok, value}
          :not_found -> GenServer.call(__MODULE__, {:fetch, key, fallback_fn})
        end
      end
    end
  end
end
