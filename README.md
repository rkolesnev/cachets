# Cachets

ETS-based replicated cache.

This is an **experimental** project. It aims to create a cache with the following properties:

* ability to create multiple caches per application (through `use Cachets`) 
* each cache is a combination of GenServer and an ETS table
* `get` - is a process-local ets read
* `put`, `pop` - are calls to GenServer, so no parallel mutations to the cache possible
* `fetch` - process local read, then GenServer based read (it allows to wait for any current changes to cache), then `put`

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cachets` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cachets, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cachets](https://hexdocs.pm/cachets).

